package ru.bislab.promodj.android.core;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.ShareActionProvider;

import ru.bislab.promodj.android.R;
import ru.bislab.promodj.android.ui.AboutActivity;
import ru.bislab.promodj.android.ui.RadioChannels;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.widget.Toast;
import net.saik0.android.unifiedpreference.UnifiedPreferenceFragment;
import net.saik0.android.unifiedpreference.UnifiedSherlockPreferenceActivity;

public class SettingActivity extends UnifiedSherlockPreferenceActivity {
	
	ActionBar actionBar;
	final int DIALOG_EXIT = 1;
	
	@Override 
	public void onCreate(Bundle savedInstanceState) {
        setHeaderRes(R.xml.pref_headers);
        setSharedPreferencesName("unified_preference_pdj");
		setSharedPreferencesMode(Context.MODE_PRIVATE);
        super.onCreate(savedInstanceState);
        
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(getResources().getString(R.string.TitleAbr_Setting));
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	   getSupportMenuInflater().inflate(R.menu.activity_setting, menu);
	   return super.onCreateOptionsMenu(menu);
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent intent = new Intent(this, RadioChannels.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			return true;
		case R.id.menuClose:
			showDialog(DIALOG_EXIT);
            return true;
        case R.id.menuAbout:
        	showAboutActivity();
        	return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	//Dialog exit
		protected Dialog onCreateDialog(int id) {
		      if (id == DIALOG_EXIT) {
		        AlertDialog.Builder adb = new AlertDialog.Builder(this);
		        //Dialog title
		        adb.setTitle(R.string.exit);
		        //Dialog msg
		        adb.setMessage(R.string.msg_exit);
		        adb.setIcon(android.R.drawable.ic_dialog_info);
		        adb.setPositiveButton(R.string.yes, ExitClickListener);
		        adb.setNegativeButton(R.string.no, ExitClickListener);
		        return adb.create();
		      }
		      return super.onCreateDialog(id);
		    }
		
		OnClickListener ExitClickListener = new OnClickListener() {
		    public void onClick(DialogInterface dialog, int which) {
		      switch (which) {
		      // Button Ok
		      case Dialog.BUTTON_POSITIVE:
		    	  shutdownActivity();
		        break;
		      // Button No
		      case Dialog.BUTTON_NEGATIVE:
		        break;
		      }
		    }
		  };
		
		 // Show About Activity
		 private void showAboutActivity() {
			Intent intent = new Intent(Intent.ACTION_VIEW);
		    intent.setClassName(this, AboutActivity.class.getName());
		    startActivity(intent);
		 }
		 
		 // Shutdown App
		 private void shutdownActivity() {
			 super.finish();
		 }
	
	
	
    public static class GeneralPreferenceFragment extends UnifiedPreferenceFragment {}
}
