package ru.bislab.promodj.android.core;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.bislab.promodj.android.R;
import ru.bislab.promodj.android.enviroment.JSONLoader;
import ru.bislab.promodj.android.enviroment.Parameter;
import android.app.Application;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.text.format.Time;
import android.util.Log;

public class PromoDJApplication extends Application {

    /** Main app package name */
    private static final String APP_PACKAGE_NAME = PromoDJApplication.class.getPackage().getName();

    /** Intent action to start the service */
    public static String ACTION_START_MEDIA_SERVICE =
            APP_PACKAGE_NAME + ".ACTION_START_MEDIA_SERVICE";

    /** Intent action to start the main activity */
    public static String ACTION_START_MAIN_ACTIVITY =
            APP_PACKAGE_NAME + ".ACTION_START_MAIN_ACTIVITY";

    /** Verbose extra name for the service start intent. */
    public static String EXTRA_VERBOSE = "verbose";
    /** Force extra name for the service start intent. */
    public static String EXTRA_FORCE = "force";

	private SharedPreferences preferences;

	private String language;

	private Locale locale;

	@Override
	  public void onCreate() {
	    super.onCreate();
	    preferences = PreferenceManager.getDefaultSharedPreferences(this);
            language = preferences.getString("lang", "default");	
            if (language.equals("default")) {language = getResources().getConfiguration().locale.getCountry();}
            locale = new Locale(language);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, null);
	    initSettings();
	  }
	
	  protected void initSettings() {
		PromoDJSettings.initInstance();
		//TODO:���� ����� ����� ����������c � �����������
	  }

       @Override
        public void onConfigurationChanged(Configuration newConfig)
        {
            super.onConfigurationChanged(newConfig);
            locale = new Locale(language);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, null);     
        }	
	
	private boolean isValidRadioChannellList() {
		try { 
			JSONObject �hannellList = PromoDJSettings.getInstance().getRadioChannellList();
			if (�hannellList.length() == 0)
				return false;
			else {
				Time time = new Time();
				time.setToNow();
			   	long nowTimeStamp = time.normalize(false)/Parameter.MILLISECONDS;
				long maxAge = �hannellList.getLong(Parameter.TAG_MAX_AGE.getLabel());
				long timestamp = �hannellList.getLong(Parameter.TAG_TIMESTAMP.getLabel());
				return ((nowTimeStamp - timestamp) > maxAge)? false:true;
			}
		} catch (JSONException e) {
			Log.e("isValidRadioChannellList",e.getMessage(), e);
		}
		return false;
	}
	
	public boolean isValidRadioPlaylList(String channelId) {
	//TODO:TIME FROM PREFERENCE
		try { 
			long TIMEPLAYLIST = Parameter.HOUR*3; //TODO: from preference
			JSONObject channelPlayList = PromoDJSettings.getInstance().getRadioPlayList(channelId);
			Time time = new Time();
			if (channelPlayList == null || channelPlayList.length() == 0)
		    	return false;
			 else {
			     time.setToNow();
			   	 long nowTimeStamp = time.normalize(false)/Parameter.MILLISECONDS;
				 long timestamp = channelPlayList.getLong(Parameter.TAG_TIMESTAMP.getLabel()); 
 				 return ((nowTimeStamp - timestamp) > TIMEPLAYLIST)? false:true;
			} 
		} catch (JSONException e) {
			Log.e("isValidRadioPlaylList",e.getMessage());
		}
		return false;
	} 

	  public Map<String,JSONObject> getRadioChannellsSettings() {
		  Map<String,JSONObject> radioChannells = new HashMap<String,JSONObject>();
		  //TODO: get from cache about cache
		  return radioChannells;
	  }
	
	  public JSONObject getRadioChannellList() {
	    	Resources appResources = getResources();
	    	String channelUrl = appResources.getString(R.string.UrlChannels);
			try {
				if (channelUrl != null) {
					if (!isValidRadioChannellList()) {
						JSONObject object = new JSONLoader().getJSONFromUrl(channelUrl);
						object.put(Parameter.TAG_TIMESTAMP.getLabel(), new Date().getTime());
						PromoDJSettings.getInstance().setRadioChannellList(object);
					} 
				} else
					throw new IllegalArgumentException("Radio channel list is absent");
			} catch (Exception e) {
				Log.e("getRadioChannellsSettings", e.getMessage());
			}
			return PromoDJSettings.getInstance().getRadioChannellList();
	  }
	  
	  public JSONObject getRadioPlayList(String channelId) throws JSONException {
	    	Resources appResources = getResources();
	        String urlPlayListChannel = appResources.getString(R.string.UrlPlaylist);
	        try {
				if (urlPlayListChannel != null) {
					if (!isValidRadioPlaylList(channelId)) {
 						JSONObject object = new JSONLoader().getJSONFromUrl(urlPlayListChannel + channelId);
 						PromoDJSettings.getInstance().setRadioPlayList(object, channelId); 
					} 
				} else
					throw new IllegalArgumentException("Radio channel list is absent");
	        	
			} catch (Exception e) {
				Log.e("getRadioPlayList", e.getMessage());
			}

			return PromoDJSettings.getInstance().getRadioPlayList(channelId);
	  }	 
	  
	  public JSONObject getChannelActualFileID(JSONObject channelSelectedItem) throws JSONException {
		 String channelId = channelSelectedItem.getString(Parameter.TAG_CHANNEL_ID.getLabel());
		 JSONObject aPlayList = getRadioPlayList(channelId);
		 JSONArray itemList = aPlayList.getJSONArray(Parameter.TAG_PLAYLIST.getLabel());
		 Time time = new Time();
		 for(int i = 0; i < itemList.length(); i ++ ) {
		     time.setToNow();
		   	 long timeStamp = time.normalize(false)/Parameter.MILLISECONDS;
			 JSONObject aFile = itemList.getJSONObject(i);
			 long fromd = aFile.getLong(Parameter.TAG_FROMD.getLabel());
			 if ( timeStamp > fromd ) return aFile;
		 }		 
		return null;
	  }

    /**
     *  Returns intent to start the main activity.
     */
    public static Intent getMainActivityIntent() {
        Intent startIntent = new Intent(ACTION_START_MAIN_ACTIVITY);
        startIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return startIntent;
    }

    /**
     *  Starts the main activity.
     */
    public static void startMainActivity(Context context) {
        context.startActivity(getMainActivityIntent());
    }
	
    @Override
	public void onTerminate() {
      super.onTerminate();
    //TODO: Check size and clean cache
    }  
    
    @Override
	public void onLowMemory() {
      super.onLowMemory();
      //TODO: Check size and clean cache
      
    }


}

