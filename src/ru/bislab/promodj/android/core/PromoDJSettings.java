package ru.bislab.promodj.android.core;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.bislab.promodj.android.enviroment.JSONLoader;
import ru.bislab.promodj.android.enviroment.Utility;

import android.util.Log;

public class PromoDJSettings {
    
	private static volatile PromoDJSettings instance = null;
	private static final JSONObject channelList = new JSONObject();
	private static final JSONObject playList = new JSONObject();
    
	private PromoDJSettings() {}

    public static PromoDJSettings getInstance() {
        // Return the instance
        return instance;
    }
    
    public static PromoDJSettings initInstance() {
            if (instance == null) {
                    synchronized (PromoDJSettings.class){
                            if (instance == null) {
                                    instance = new PromoDJSettings ();
                            }
                  }
            }
            return instance;
    }

	public JSONObject getRadioPlayList(String channelId) throws JSONException {
		return playList.getJSONObject(channelId);
	}
	
	public void setRadioPlayList(JSONObject aObject, String channelId) throws JSONException {
		playList.putOpt(channelId, aObject);
	}

	public JSONObject getRadioChannellList() {
		return channelList;
	}
	
	public void setRadioChannellList(JSONObject aObject) throws JSONException {
		Utility.copyJSONObject(channelList, aObject);
	}
}
