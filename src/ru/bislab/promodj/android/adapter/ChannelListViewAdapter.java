package ru.bislab.promodj.android.adapter;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import com.actionbarsherlock.app.SherlockExpandableListActivity;
import com.androidquery.AQuery;
import android.util.Log;
import ru.bislab.promodj.android.R;
import ru.bislab.promodj.android.core.PromoDJApplication;
import ru.bislab.promodj.android.enviroment.Parameter;
import ru.bislab.promodj.android.enviroment.Utility;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ListView;
import android.app.Activity;
import android.app.Application;
import ru.bislab.promodj.android.view.RadioActivityView;

public class ChannelListViewAdapter  extends BaseExpandableListAdapter {

	private Context mContext;
    private JSONArray itemList;
    private static LayoutInflater inflater=null;
    SherlockExpandableListActivity mRadioChannels;
    PromoDJApplication  application;
    
    
	public ChannelListViewAdapter(Context context,
			JSONObject aRadioChannels, Application  aApplication) throws JSONException {
		itemList = aRadioChannels.getJSONArray(Parameter.TAG_CHANNELS.getLabel());
		mContext = context; 
		inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.application = (PromoDJApplication) aApplication;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return null;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		RadioActivityView radioView = new RadioActivityView(mContext);
		try {
			RadioPlayerAdapter radioAdapter = new RadioPlayerAdapter(mContext, convertView, parent); 
			JSONObject channel = getGroup(groupPosition);
			JSONObject instantFileID = application.getChannelActualFileID(channel);
			radioAdapter.setItem(instantFileID);
			radioAdapter.setChannel(getGroup(groupPosition));
			radioView.setAdapter(radioAdapter);
		} catch (JSONException e) {
			Log.e("getGroup", e.getMessage());
		}
		radioView.setGroupIndicator(null);
		return radioView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
        JSONObject item = null;
        int length = 0;
		try {
			item = (JSONObject)itemList.get(groupPosition);
			length = item.length();
		} catch (JSONException e) {
		Log.e("getGroup", e.getMessage());
		}
		 return length; 
	}

	@Override
	public JSONObject getGroup(int groupPosition) {
		JSONObject item = null;
		try {
			item = (JSONObject) itemList.get(groupPosition);
		} catch (JSONException e) {
		Log.e("getGroup", e.getMessage());
		}
		 return item; 
	}

	@Override
	public int getGroupCount() {
		return itemList.length();
	}

	@Override
	public long getGroupId(int groupPosition) {
        JSONObject item = null;
        long id = 0;
		try {
			item = (JSONObject)itemList.get(groupPosition);
			id = item.getLong(Parameter.TAG_CHANNEL_ID.getLabel());
		} catch (JSONException e) {
		Log.e("getGroup", e.getMessage());
		}
		 return id; 
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View view, ViewGroup viewGroup) {
        try { 
       	 if (view == null) 
       		view = createView(viewGroup);
			bindView(view, groupPosition);
		} catch (JSONException e) {
			Log.e("getGroupView", e.getMessage());
		}
       
       if ( groupPosition == getGroupCount())
       	Utility.setListViewHeightBasedOnChildren((ListView) view);
       return view;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
	}
	
	  View createView(ViewGroup viewGroup) {
	        return inflater.inflate(R.layout.radio_channels_v2, viewGroup, false);
	  }
	void bindView(View view, int i) throws JSONException {
		try {
			JSONObject item = itemList.getJSONObject(i);
	        String title = item.getString(Parameter.TAG_TITLE.getLabel());
	        String style = item.getString(Parameter.TAG_CHANNEL_STYLE.getLabel());
			//Set aQuery value
			AQuery aQuery = new AQuery(view);
	        aQuery.id(R.id.tvNameChannel).text(title)
	        	.typeface(Typeface.createFromAsset(view.getContext().getAssets(), Parameter.FONT_REGULAR));
	        aQuery.id(R.id.tvStylesChannel).text(style)
	        	.typeface(Typeface.createFromAsset(view.getContext().getAssets(), Parameter.FONT_LIGHT));
		} catch (JSONException e) {	}	
	}
}