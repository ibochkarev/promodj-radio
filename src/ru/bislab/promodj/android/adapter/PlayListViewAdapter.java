package ru.bislab.promodj.android.adapter;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import ru.bislab.promodj.android.R;
import ru.bislab.promodj.android.enviroment.Parameter;
import ru.bislab.promodj.android.enviroment.Utility;
import ru.bislab.promodj.android.ui.PlaylistActivity;
import ru.bislab.promodj.android.ui.PlaylistActivity.RadioPlaylList;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.util.AQUtility;
import android.text.format.DateUtils;
import android.text.format.Time;

public class PlayListViewAdapter extends BaseAdapter {
    
	private Activity activity;
    private JSONArray itemList;
    private List<JSONObject> objList;
    private static LayoutInflater inflater;
    
    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
    
	public PlayListViewAdapter(ListActivity playlistActivity,
			JSONObject aPlayList) throws JSONException {
		activity = playlistActivity;
		itemList = aPlayList.getJSONArray(Parameter.TAG_PLAYLIST.getLabel());
		objList = Utility.convertJSONArrayToOrderedList(itemList);
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
  
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return itemList.length();
	}
	
	@Override
    public Object getItem(int i) {
      Object item = objList.get(i);
	 return item;
    }
    
    @Override
    public long getItemId(int i) {
        return i;
    }
    
	@Override
	public View getView(int i, View view, ViewGroup viewGroup) {
        try { 
        	 if (view == null) 
        		 	view = createView(viewGroup);
        	 bindView(view, objList.get(i));
		} catch (JSONException e) {	}
        	
        return view;
	}
	
    View createView(ViewGroup viewGroup) {
        return inflater.inflate(R.layout.list_row, viewGroup, false);
    }
    
    void bindView(View view, JSONObject item) throws JSONException {
    	try {
    		Time time = new Time();
    		time.setToNow();
    		long currentTime =  time.normalize(false);
	        item.getString(Parameter.TAG_FILEID.getLabel());
	        long fromTime = Long.parseLong(item.getString(Parameter.TAG_FROMD.getLabel()))*Parameter.MILLISECONDS;
	        long tillTime = Long.parseLong(item.getString(Parameter.TAG_TILLD.getLabel()))*Parameter.MILLISECONDS;
	        dateFormat.format(new Date(tillTime));
	        String title = item.getString(Parameter.TAG_TITLE.getLabel());
	        item.getString(Parameter.TAG_URL.getLabel());
	        item.getString(Parameter.TAG_DOWNLOAD.getLabel());
	        String thumbnail = item.getString(Parameter.TAG_AVATAR.getLabel());

    		AQuery aQuery = new AQuery(view);
    		Bitmap preset = aQuery.getCachedImage(thumbnail);
	        aQuery.id(R.id.tvPlTitle).text(title)
      	        	.typeface(Typeface.createFromAsset(view.getContext().getAssets(), Parameter.FONT_REGULAR));
	        String fromD  = (String) DateUtils.getRelativeTimeSpanString(fromTime, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
	        aQuery.id(R.id.tvPlFromD).text(fromD)
	        		.typeface(Typeface.createFromAsset(view.getContext().getAssets(), Parameter.FONT_REGULAR));
	        aQuery.id(R.id.list_avatar).image(thumbnail, false, true, 0, 0, preset, AQuery.FADE_IN);
	        if (fromTime > currentTime) return;
	        
    	} catch (JSONException e) {
    		Log.e("PlayListViewAdapter bindView", e.toString());
    	}

   }
    
    
}