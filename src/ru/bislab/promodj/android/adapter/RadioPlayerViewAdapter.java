package ru.bislab.promodj.android.adapter;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import ru.bislab.promodj.android.R;
import ru.bislab.promodj.android.enviroment.Parameter;
import ru.bislab.promodj.android.enviroment.Utility;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Environment;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.BitmapAjaxCallback;
import com.androidquery.util.AQUtility;

public class RadioPlayerViewAdapter extends BaseAdapter {

	private Activity activity;
    private JSONArray itemList;
    private List<JSONObject> objList;
    private JSONObject mChannel;
    private static LayoutInflater inflater=null;

    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

	public RadioPlayerViewAdapter(Activity playerActivity,
			JSONObject aChannel, JSONObject aPlayList) throws JSONException {
		
		itemList = aPlayList.getJSONArray(Parameter.TAG_PLAYLIST.getLabel());
		objList = convertJSONArrayToList(itemList);
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mChannel = aChannel;
		activity = playerActivity;
        String ext = Environment.getExternalStorageDirectory().getAbsolutePath();
        File cacheDir = new File(ext, "data/pdj/temp");
        AQUtility.setCacheDir(cacheDir);
	}

    public static List<JSONObject> convertJSONArrayToList(JSONArray aArray) throws JSONException {
    	LinkedList<JSONObject> orderList = new LinkedList<JSONObject>();

    	if (aArray != null)
    		for(int i = 0; i < aArray.length(); i++){
    			JSONObject channel = aArray.getJSONObject(i);
    			orderList.addFirst(channel);
    		}
        return orderList;
    }

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return itemList.length();
	}

	@Override
    public Object getItem(int i) {
      Object item = objList.get(i);
	 return item;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

	@Override
	public View getView(int i, View view, ViewGroup viewGroup) {
        try {
        	 if (view == null)
        		view = createView(viewGroup);
			bindView(view, i);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       
        return view;
	}

    View createView(ViewGroup viewGroup) {
        return inflater.inflate(R.layout.list_row, viewGroup, false);
    }


    void bindViewChanel(View view, int i) throws JSONException {
    	try{
    		JSONObject item = objList.get(i);
	        String logo = item.getString(Parameter.TAG_LOGO.getLabel());
    		String title = item.getString(Parameter.TAG_TITLE.getLabel());
    		String styles = item.getString(Parameter.TAG_CHANNEL_STYLE.getLabel());
    		String description = item.getString(Parameter.TAG_DESC.getLabel());
    		
    		AQuery aQuery = new AQuery(view);
    		Bitmap preset = aQuery.getCachedImage(logo);
    		
    		aQuery.id(R.id.TvNameChannel).text(title);
    		// Set tv Logo channel
	        aQuery.id(R.id.IvLogoChannel).image(logo, false, true, 0, 0, preset, AQuery.FADE_IN);
	        // Set tv styles channel
	        aQuery.id(R.id.TvChannelStyle).text(styles)
	        		.typeface(Typeface.createFromAsset(view.getContext().getAssets(), Parameter.FONT_LIGHT));
	        // Set tv desc channel
	        aQuery.id(R.id.TvChannelDesc).text(description);
	        return; 
    	} catch (JSONException e) {
    		Log.e("RadioPlayerViewAdapter bindView", e.toString());
    	}
   }

    void bindView(View view, int i) throws JSONException {

    	try {
    		JSONObject item = objList.get(i);
    		Time time = new Time();
    		time.setToNow();
    		long currentTime =  time.normalize(false)/Parameter.MILLISECONDS;
	        item.getString(Parameter.TAG_FILEID.getLabel());
	        long fromTime = Long.parseLong(item.getString(Parameter.TAG_FROMD.getLabel()))*Parameter.MILLISECONDS;
	        long tillTime = Long.parseLong(item.getString(Parameter.TAG_TILLD.getLabel()))*Parameter.MILLISECONDS;
	        dateFormat.format(new Date(tillTime));
	        String tillD = dateFormat.format(new Date(tillTime));
	        String title = item.getString(Parameter.TAG_TITLE.getLabel());
	        String url = item.getString(Parameter.TAG_URL.getLabel());
	        String download = item.getString(Parameter.TAG_DOWNLOAD.getLabel());
	        String thumbnail = item.getString(Parameter.TAG_AVATAR.getLabel());

    		AQuery aQuery = new AQuery(view);
    		Bitmap preset = aQuery.getCachedImage(thumbnail);
    		//Set aQuery tv
	        aQuery.id(R.id.tvPlTitle).text(title)
      	        	.typeface(Typeface.createFromAsset(view.getContext().getAssets(), Parameter.FONT_REGULAR));
	        String fromD  = (String) DateUtils.getRelativeTimeSpanString(fromTime, System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
	        
	        aQuery.id(R.id.tvPlFromD).text(fromD).typeface(Typeface.createFromAsset(view.getContext().getAssets(), Parameter.FONT_REGULAR));
	        aQuery.id(R.id.list_avatar).image(thumbnail, false, true, 0, 0, preset, AQuery.FADE_IN);

	        if (fromTime > currentTime) return;

    	} catch (JSONException e) {
    		Log.e("RadioPlayerViewAdapter bindView", e.toString());
    	}

   }


}