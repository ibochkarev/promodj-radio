package ru.bislab.promodj.android.adapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.bislab.promodj.android.R;
import ru.bislab.promodj.android.enviroment.Parameter;
import ru.bislab.promodj.android.enviroment.Utility;

import com.actionbarsherlock.app.SherlockExpandableListActivity;
import com.androidquery.AQuery;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ListView;

public class RadioPlayerAdapter extends BaseExpandableListAdapter {

	private Context mContext;
    private JSONObject radioChannel;
    private JSONObject item;
    private static LayoutInflater inflater=null;
    SherlockExpandableListActivity mRadioChannels;

	public RadioPlayerAdapter(Context context, View convertView, ViewGroup parent) throws JSONException {
		mContext = context; 
		inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setChannel(JSONObject aRadioChannel) {
	    radioChannel = aRadioChannel; 
	}
	
	public void setItem(JSONObject aItem) {
		item = aItem;
	}
	
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
  /*
   ViewGroup container = (ViewGroup)findViewById(R.id.container);
for (int i = 0; i < 6; i++) {
    View myLayout = getLayoutInflater.inflate(R.layout.mylayout, null);
    TextView tv = myLayout.findViewById(R.id.textView);
    tv.setText("my layout " + i);
    container.addView(myLayout); // you can pass extra layout params here too
}
   * */
		try {
        	 if (convertView == null)
        		convertView = createView(parent);
        	 	bindView(parent);
		} catch (Exception e) {
			e.printStackTrace();
		}

         return convertView;
	}

    private void bindView(ViewGroup parent) throws JSONException {
    	 String tvNameChannel  = radioChannel.getString(Parameter.TAG_TITLE.getLabel());
    	 String tvChannelStyle = radioChannel.getString(Parameter.TAG_CHANNEL_STYLE.getLabel());
    	 String tvStyleCurrentTrack = radioChannel.getString(Parameter.TAG_DESC.getLabel()); 
    	 String tvTitleCurrentTrack  = item.getString(Parameter.TAG_TITLE.getLabel());
         String thumbnail = item.getString(Parameter.TAG_LOGO.getLabel());
         AQuery aQuery = new AQuery(parent);
         Bitmap preset = aQuery.getCachedImage(thumbnail);
         aQuery.id(R.id.list_avatar).image(thumbnail, false, true, 0, 0, preset, AQuery.FADE_IN);

         Button btDownload = (Button)aQuery.id(R.id.BtDownload).getButton();
         
         Button BtPlaylist = (Button)aQuery.id(R.id.BtPlaylist).getButton();
   	 
	}

	View createView(ViewGroup viewGroup) {
        return inflater.inflate(R.layout.act_radiochannel_v2, viewGroup, false);
    }
    
	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return 0;
	}
}
