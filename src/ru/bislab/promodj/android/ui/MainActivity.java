package ru.bislab.promodj.android.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(getIntent());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        /*
        intent.setClass(this, SplashActivity2.class);
        try {
            if (Integer.parseInt(Build.VERSION.SDK) >= 14) {
                intent.setClass(this, SplashActivity4.class);
            }
        } catch (NumberFormatException e) {
            //pass to MainActivity2
        }*/
        startActivity(intent);
        finish();
    }
    
}