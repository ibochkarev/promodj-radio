package ru.bislab.promodj.android.ui;

import com.actionbarsherlock.app.SherlockActivity;

import ru.bislab.promodj.android.R;

import android.os.Bundle;

public class AboutActivity extends SherlockActivity {
	/**
	 * @see android.app.Activity#onCreate(Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_layout);
	}
}
