package ru.bislab.promodj.android.ui;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockExpandableListActivity;
import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.ShareActionProvider;

import ru.bislab.promodj.android.R;
import ru.bislab.promodj.android.adapter.ChannelListViewAdapter;
import ru.bislab.promodj.android.core.PromoDJApplication;
import ru.bislab.promodj.android.core.SettingActivity;
import ru.bislab.promodj.android.enviroment.Parameter;
import ru.bislab.promodj.android.enviroment.Utility;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;


public class RadioChannels extends SherlockExpandableListActivity {

	ActionBar actionBar;
	
	//TAG Log
    final String TAG = "States";
    // Are you going to use this constants in other parts of application?
    final int DIALOG_EXIT = 1;

    private ShareActionProvider mShareActionProvider;
    private ExpandableListView lvRadioChannels = null;
    private RadioChannelllList radioList;
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
	    lvRadioChannels = (ExpandableListView)findViewById(android.R.id.list);
		if (lvRadioChannels != null){
	    	radioList =	new RadioChannelllList(lvRadioChannels);
			radioList.execute();
		}
    }
		
    public class RadioChannelllList extends AsyncTask<String, Void, JSONObject> {
    	
    	private ExpandableListView lvRadioChannels;
    	
		public RadioChannelllList(final ExpandableListView lvRadioChannels ) {
			 this.lvRadioChannels = lvRadioChannels;
		}

		@Override
		protected JSONObject doInBackground(String... params) {
			try {
				return ((PromoDJApplication)getApplication()).getRadioChannellList();
			} catch (Exception e) {
				Log.e("doInBackground", e.getMessage());
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(JSONObject aRadioChannel) {
			super.onPostExecute(aRadioChannel);
			try {
				ChannelListViewAdapter adapter = new ChannelListViewAdapter(getApplicationContext(),
						aRadioChannel, RadioChannels.this.getApplication());
				lvRadioChannels.setAdapter(adapter);
			} catch (JSONException e) {
				Log.e("onPostExecute",e.toString());
			}
			
			lvRadioChannels.setOnGroupExpandListener (new ExpandableListView.OnGroupExpandListener () {
				@Override
				public void onGroupExpand(int groupPosition) {
				}
			}); 
			
			lvRadioChannels.setOnGroupCollapseListener (new ExpandableListView.OnGroupCollapseListener () {
				@Override
				public void onGroupCollapse(int groupPosition) {
					
					
				}
			}); 
			
			lvRadioChannels.setOnChildClickListener (new ExpandableListView.OnChildClickListener() {
				@Override
				public boolean onChildClick(ExpandableListView parent, 
											View v, int groupPosition, int childPosition, long id) {
					return false;
				}
			});
			

		}
    } 

	//Create menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	   getSupportMenuInflater().inflate(R.menu.activity_main, menu);
	   /** Getting the actionprovider associated with the menu item whose id is share */
        mShareActionProvider = (ShareActionProvider) menu.findItem(R.id.menuShare).getActionProvider();
 
        /** Getting the target intent */
        Intent intent = getDefaultShareIntent();
 
        /** Setting a share intent */
        if(intent!=null)
            mShareActionProvider.setShareIntent(intent);
	   return super.onCreateOptionsMenu(menu);
	}
	
	/** Returns a share intent */
    private Intent getDefaultShareIntent(){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "SUBJECT");
        intent.putExtra(Intent.EXTRA_TEXT,"Sample Content !!!");
        return intent;
    }
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
	          // Handle item selection
			if (item.getItemId() == R.id.menuClose){
				showDialog(DIALOG_EXIT);
                return true;
			} else if (item.getItemId() == R.id.menuAbout){
                showAboutActivity();
                return true;
			} else if (item.getItemId() == R.id.menuSettings){
				showSettinsActivity();
				return true;				
			}
          return super.onOptionsItemSelected(item);
	}
	
	private void showSettinsActivity() {
		Intent intent = new Intent(Intent.ACTION_VIEW);
	    intent.setClassName(this, SettingActivity.class.getName());
	    startActivity(intent);
		
	}

	//Dialog exit
	protected Dialog onCreateDialog(int id) {
	      if (id == DIALOG_EXIT) {
	        AlertDialog.Builder adb = new AlertDialog.Builder(this);
	        //Dialog title
	        adb.setTitle(R.string.exit);
	        //Dialog msg
	        adb.setMessage(R.string.msg_exit);
	        adb.setIcon(android.R.drawable.ic_dialog_info);
	        adb.setPositiveButton(R.string.yes, ExitClickListener);
	        adb.setNegativeButton(R.string.no, ExitClickListener);
	        return adb.create();
	      }
	      return super.onCreateDialog(id);
	    }
	
	OnClickListener ExitClickListener = new OnClickListener() {
	    public void onClick(DialogInterface dialog, int which) {
	      switch (which) {
	      // Button Ok
	      case Dialog.BUTTON_POSITIVE:
	    	  shutdownActivity();
	        break;
	      // Button No
	      case Dialog.BUTTON_NEGATIVE:
	        break;
	      }
	    }
	  };
	
	 // Show About Activity
	 private void showAboutActivity() {
		Intent intent = new Intent(Intent.ACTION_VIEW);
	    intent.setClassName(this, AboutActivity.class.getName());
	    startActivity(intent);
	 }
	 
	 // Shutdown App
	 private void shutdownActivity() {
		 super.finish();
	 }
	    
	@Override
	  protected void onRestart() {
	    super.onRestart();
	    Log.d(TAG, "RadioChannels: onRestart()");
	  }
	
      @Override
      protected void onStart() {
        super.onStart();
        Log.d(TAG, "RadioChannels: onStart()");
      }

      @Override
      protected void onResume() {
        super.onResume();
        Log.d(TAG, "RadioChannels: onResume()");
      }

      @Override
      protected void onPause() {
        super.onPause();
        Log.d(TAG, "RadioChannels: onPause()");
      }

      @Override
      protected void onStop() {
        super.onStop();
        Log.d(TAG, "RadioChannels: onStop()");
      }

      @Override
      protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "RadioChannels: onDestroy()");
      }
}
