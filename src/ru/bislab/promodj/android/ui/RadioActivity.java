package ru.bislab.promodj.android.ui;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

//import ru.bislab.promodj.android.CONSTANTS;
import ru.bislab.promodj.android.R;

import ru.bislab.promodj.android.adapter.PlayListViewAdapter;
import ru.bislab.promodj.android.adapter.RadioPlayerViewAdapter;
import ru.bislab.promodj.android.core.PromoDJApplication;
import ru.bislab.promodj.android.enviroment.Parameter;
import ru.bislab.promodj.android.media.mediaplayer.IMediaPlayerServiceClient;
import ru.bislab.promodj.android.media.mediaplayer.MediaPlayerService;
import ru.bislab.promodj.android.media.mediaplayer.StatefulMediaPlayer;
import ru.bislab.promodj.android.media.mediaplayer.MediaPlayerService.MediaPlayerBinder;
import ru.bislab.promodj.android.ui.PlaylistActivity.RadioPlaylList;
import ru.bislab.promodj.audio.media.streamStation.StreamStation;
import ru.bislab.promodj.audio.media.streamStation.StreamStationSpinnerAdapter;


public class RadioActivity extends SherlockActivity implements IMediaPlayerServiceClient
{
	private StatefulMediaPlayer mMediaPlayer;
	private MediaPlayerService mService;
	private boolean mBound;
	private JSONObject instantFileID =  null;
	private ProgressDialog mProgressDialog;
	public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
	private JSONObject selectedItem;
	private JSONObject playListChannel;
	private PromoDJApplication application;
	private StreamStation mSelectedStream = null;
	
	//initialize root directory
    File rootDir = Environment.getExternalStorageDirectory();
    public String fileName = "codeofaninja.jpg";
    public String fileURL = "https://lh4.googleusercontent.com/-HiJOyupc-tQ/TgnDx1_HDzI/AAAAAAAAAWo/DEeOtnRimak/s800/DSC04158.JPG";
		
	ActionBar actionBar;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.radioplayer_activity);
		actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(getResources().getString(R.string.TitleAbr_Player));
		
		try {
			application = (PromoDJApplication) getApplication();
	        Bundle extras = getIntent().getExtras();
	        //���������� ������ ������ ����� ���������� - ������� ������
	        //Define source where from  was did call this functionality  
	        if (extras.containsKey(Parameter.TAG_CHANNEL_ID.getLabel())) {
		        selectedItem = new JSONObject(extras.get(Parameter.TAG_CHANNEL_ID.getLabel()).toString());
	        	instantFileID = application.getChannelActualFileID(selectedItem);
	        	playListChannel = application.getRadioPlayList(selectedItem.getString(Parameter.TAG_CHANNEL_ID.getLabel()));
	        	mSelectedStream =  getStreamStation(selectedItem);
	        } else 
	        	throw new IllegalArgumentException(" Channel is absent");

	    	if (mSelectedStream != null)
	    	{
		        bindToService();
				mProgressDialog = new ProgressDialog(this);	
			// ������������������ ����� ������ ��� ������ ���������� ��� ���������������� ������ mService	
	    	//	mService.stopMediaPlayer();
	    	//	mService.initializePlayer(mSelectedStream);
		    	View lvRadioPlayer = findViewById(android.R.id.list);
		    	if (lvRadioPlayer != null ){
		    		RadioPlayer player = new RadioPlayer(lvRadioPlayer, selectedItem, instantFileID);
		        	player.execute();
		    	}  
	    	}

		} catch (JSONException e) {
			Log.e("onCreate", e.getMessage());
		}
    }
	
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			   getSupportMenuInflater().inflate(R.menu.activity_radioplayer, menu);
			   return super.onCreateOptionsMenu(menu);
		}
		
		// Item Selected Menu Home Icon
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			switch (item.getItemId()) {
			case android.R.id.home:
				Intent intent = new Intent(this, RadioChannels.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				return true;
			case R.id.menuQuality:
				Toast.makeText(this, "menuQuality selected", Toast.LENGTH_SHORT).show();
				return true;
			case R.id.menuAllDownload:
				Toast.makeText(this, "menuAllDownload selected", Toast.LENGTH_SHORT).show();
				return true;
			default:
				return super.onOptionsItemSelected(item);
			}
		}

 public class RadioPlayer extends AsyncTask<String, Void, JSONObject> {
   		 
    	private View lvRadioPlayer;
    	private JSONObject selectedChannel;
    	private JSONObject instantFileID;
    	
		public RadioPlayer(View lvRadioPlayer, JSONObject selectedChannel, JSONObject instantFileID ) {
    		this.lvRadioPlayer = lvRadioPlayer;
    		this.selectedChannel = selectedChannel;
    		this.instantFileID = instantFileID;  		
		}
		@Override
		protected JSONObject doInBackground(String... params) {
			try {
				  String channelId =  selectedChannel.getString(Parameter.TAG_CHANNEL_ID.getLabel());
					if (channelId != null)
						return ((PromoDJApplication)getApplication()).getRadioPlayList(channelId);
			} catch (Exception e) {
				Log.e("doInBackground", e.getMessage());
			}
			return null;
		}
			
		@Override
    	protected void onPostExecute(JSONObject aPlayList) {
			super.onPostExecute(aPlayList);
			try {
				RadioPlayerViewAdapter adapter = new RadioPlayerViewAdapter(RadioActivity.this, selectedChannel, aPlayList);
				((ListView) lvRadioPlayer).setAdapter(adapter);
			} catch (JSONException e) {}	 
		}
    }

	/**
	 * Binds to the instance of MediaPlayerService. If no instance of MediaPlayerService exists, it first starts
	 * a new instance of the service.
	 */
	public void bindToService()
	{
     Intent intent = new Intent(this, MediaPlayerService.class);
		
     if (mediaPlayerServiceRunning())
     {
			// Bind to Service
	        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
		}
     //no instance of service
		else
		{
			//start service and bind to it
			startService(intent);
	        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
	  //  ,, ����������� ��� ������������ ������� ����� ��������� mService ������������������ !!!
	    //    mConnection.onServiceConnected(name, service)
		}
		
	}
	
	/**
	 * Initializes buttons by setting even handlers and listeners, etc.
	 */
	private void initializeButtons()
	{
		// PLAY/PAUSE BUTTON
		final ToggleButton playPauseButton = (ToggleButton) findViewById(R.id.playPauseButton);		
		playPauseButton.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				if (mBound) {
					mMediaPlayer = mService.getMediaPlayer();
					
					//pressed pause ->pause
					if (!playPauseButton.isChecked())
					{
					
						if (mMediaPlayer.isStarted())
						{
							mService.pauseMediaPlayer();
						}
						
					}
					
					//pressed play
					else if (playPauseButton.isChecked())
					{
						// STOPPED, CREATED, EMPTY, -> initialize
						if (mMediaPlayer.isStopped()
								|| mMediaPlayer.isCreated()
								|| mMediaPlayer.isEmpty())
						{
							mService.initializePlayer(mSelectedStream);
						}
						
						//prepared, paused -> resume play
						else if (mMediaPlayer.isPrepared()
								|| mMediaPlayer.isPaused())
						{
							mService.startMediaPlayer();
						}
						
					}
				}
			}

		});
	}
	
	private ServiceConnection mConnection = new ServiceConnection() {
		
    	@Override
        public void onServiceConnected(ComponentName className, IBinder serviceBinder)
    	{
    		Log.d("MainActivity","service connected");

            //bound with Service. get Service instance
            MediaPlayerBinder binder = (MediaPlayerService.MediaPlayerBinder) serviceBinder;
            mService = binder.getService();
            
            //send this instance to the service, so it can make callbacks on this instance as a client
            mService.setClient(RadioActivity.this);
            mBound = true;
            
            //Set play/pause button to reflect state of the service's contained player
            final ToggleButton playPauseButton = (ToggleButton) findViewById(R.id.playPauseButton);
    		playPauseButton.setChecked(mService.getMediaPlayer().isPlaying());
    		
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0)
        {
            mBound = false;
            mService = null;
        }
    };
    
    private boolean mediaPlayerServiceRunning()
    {
    	ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
    	for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("ru.bislab.promodj.audio.media.mediaplayer.MediaPlayerService".equals(service.service.getClassName())){
                return true;
            }
        }
        return false;
    }
    
    public void onInitializePlayerSuccess()
    {
		mProgressDialog.dismiss();
		final ToggleButton playPauseButton = (ToggleButton) findViewById(R.id.playPauseButton);		
		playPauseButton.setChecked(true);
	}
    
    public void onInitializePlayerStart()
    {
		mProgressDialog = ProgressDialog.show(this, "Radio", "Connecting...", true);
		mProgressDialog.getWindow().setGravity(Gravity.TOP);
		mProgressDialog.setCancelable(true);
		mProgressDialog.setOnCancelListener(new OnCancelListener()	{
			@Override
			public void onCancel(DialogInterface dialogInterface)
			{
					RadioActivity.this.mService.resetMediaPlayer();
					final ToggleButton playPauseButton = (ToggleButton) findViewById(R.id.playPauseButton);		
					playPauseButton.setChecked(false);
			}
		});
	}
    
	
    
	@Override
	public void onError() {
			mProgressDialog.cancel();
	}
    
    public void shutdownActivity() {
    	if (mBound) {
			mService.stopMediaPlayer();
			unbindService(mConnection);
			mBound = false;
    	 }
        Intent intent = new Intent(this, MediaPlayerService.class);
    	stopService(intent);
    	finish();
    }
    
    @Override
    public void onDestroy()
    {
    	super.onDestroy();
    	if (mBound)
    	{
    		mService.unRegister();
    		unbindService(mConnection);
    		mBound = false;
    	}
    }

	private StreamStation getStreamStation(JSONObject selectedItem) {
		Map<String,JSONObject> aSetting =  ((PromoDJApplication) getApplication()).getRadioChannellsSettings();
		try{
			//-----����� ���� ���������� ����� � ��� ��������---------------------------------------------
	    	JSONObject settings = aSetting.get(selectedItem.get(Parameter.TAG_CHANNEL_ID.getLabel()));
	    	String quality = (settings != null)?settings.getString(Parameter.TAG_QUALITY.getLabel()):null;
	    	quality = (quality != null)?quality:Parameter.TAG_CHANNEL_LQ.getLabel();

	    	//--------------------------------------------------------------------------------------------
	    	String urlQuality = (Parameter.TAG_CHANNEL_HQ.getLabel().equals(quality))? 
	    				selectedItem.getString(Parameter.TAG_CHANNEL_HQ.getLabel()):
	    				selectedItem.getString(Parameter.TAG_CHANNEL_LQ.getLabel());
	    	return new StreamStation(selectedItem.getString(Parameter.TAG_TITLE.getLabel()), urlQuality);			
		}catch(Exception ex){}
		return null;
	}

	// ���� �� ������� �� ��� ���� ��� ������� ������ ����� ����� ���� � ��� �������
	// ������� ������ � Application
	public List<StreamStation> getAllStreamStations(){
		JSONObject aRadioChannells =  ((PromoDJApplication) getApplication()).getRadioChannellList();
		Map<String,JSONObject> aSetting =  ((PromoDJApplication) getApplication()).getRadioChannellsSettings();
		List<StreamStation> listChannel = new ArrayList<StreamStation>();
		try{
		JSONArray channelList = aRadioChannells.getJSONArray(Parameter.TAG_CHANNELS.getLabel());
		
	    for(int i = 0; i < channelList.length(); i++){
	    	JSONObject channel = channelList.getJSONObject(i);
	    	//-----����� ���� ���������� ����� � ��� ��������---------------------------------------------
	    	JSONObject settings = aSetting.get(channel.get(Parameter.TAG_CHANNEL_ID.getLabel()));
	    	String quality = (settings != null)?settings.getString(Parameter.TAG_QUALITY.getLabel()):null;
	    	quality = (quality != null)?quality:Parameter.TAG_CHANNEL_LQ.getLabel();
	    	//--------------------------------------------------------------------------------------------
	    	String urlQuality = (Parameter.TAG_CHANNEL_HQ.getLabel().equals(quality))? 
	    										channel.getString(Parameter.TAG_CHANNEL_HQ.getLabel()):
	    										channel.getString(Parameter.TAG_CHANNEL_LQ.getLabel());
	    	StreamStation station = new StreamStation(channel.getString(Parameter.TAG_TITLE.getLabel()), urlQuality);
	    	listChannel.add(station);
	    }
		}catch(Exception ex){}
		return listChannel;
		
	}
}



