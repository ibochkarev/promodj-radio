package ru.bislab.promodj.android.ui;


import com.androidquery.AQuery;

import ru.bislab.promodj.android.R;
import android.app.Activity;
import android.content.Intent;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import ru.bislab.promodj.android.core.PromoDJApplication;
import ru.bislab.promodj.android.enviroment.AlertDialogManager;
import ru.bislab.promodj.android.enviroment.Utility;
 
public class SplashActivity extends Activity {
 
   private static String TAG = SplashActivity.class.getName();
   private static long SLEEP_TIME = 2;    // Sleep for some time
 
   @Override
   protected final void onCreate(Bundle savedInstanceState) {
	   super.onCreate(savedInstanceState);
       AQuery aq = new AQuery(this);
 
     
      // Animation
      aq.overridePendingTransition5(android.R.anim.fade_in, android.R.anim.fade_out); 
      aq.hardwareAccelerated11();
      
     try {  
	       StrictMode.ThreadPolicy policy = null;
	       Class clazz = Class.forName("android.os.StrictMode", false, this.getClass().getClassLoader());
	       policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	       StrictMode.setThreadPolicy(policy);  
	   	} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
      // Removes title bar
      this.requestWindowFeature(Window.FEATURE_NO_TITLE);    
      // Removes notification bar
      this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);    
      //Set layout
      setContentView(R.layout.splash);
      //Check Internet Connection
      ConnectionManager connectDetector = new ConnectionManager(getApplicationContext());
      // Check if Internet present
      if (!connectDetector.isConnectingToInternet()) {
          // Internet Connection is not present
    	  new AlertDialogManager().showAlertDialog(SplashActivity.this, "��� ����������",
                  "���������. ������������ � ���������", false); // �����������
          // stop executing code by return
          return;
      } 
      // If Ok -> Start App
      if(NetworkInfo.State.CONNECTED != null){ 
	      // Start timer and launch main activity 
	      IntentLauncher launcher = new IntentLauncher();
	      launcher.start();
	      return;
		}

      }
  
   private class IntentLauncher extends Thread {
	      @Override
	      public void run() {
	         try {
	            // Sleeping
	            Thread.sleep(SLEEP_TIME*1000);
	         } catch (Exception e) {
	            Log.e(TAG, e.getMessage());
	         }
	         // Start Activity         
	         Intent intent = new Intent(SplashActivity.this, RadioChannels.class);
	         SplashActivity.this.startActivity(intent);
	  }
	}

   //Life Status Activity
   @Override
   protected void onPause() {
     super.onPause();
     finish();
     Log.d(TAG, "SplashActivity: finish()");
   }
}