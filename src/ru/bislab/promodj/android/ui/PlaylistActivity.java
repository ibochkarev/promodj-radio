package ru.bislab.promodj.android.ui;


import java.util.LinkedList;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;

import ru.bislab.promodj.android.R;
import ru.bislab.promodj.android.adapter.PlayListViewAdapter;
import ru.bislab.promodj.android.core.PromoDJApplication;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;
import ru.bislab.promodj.android.enviroment.Parameter;

public class PlaylistActivity extends SherlockListActivity {

    //TAG status
    final String TAG = "States";
    private ProgressDialog mProgressDialog;  
    private static ListView lvRadioPlayList;
    private static SimpleDateFormat sdf =  new SimpleDateFormat("HH:mm");
	
	final String[] allowedContentTypes = new String[] { 
			"audio/mpeg", "audio/mpeg3", 
			"audio/x-mpeg", "audio/x-mpeg-3", 
			"audio/mp3", "audio/3gpp", 
			"audio/amr", "audio/mid", 
			"audio/midi", "audio/mp4", 
			"audio/mpeg4", "audio/sp-midi", 
			"audio/x-mid", "audio/x-midi", 
			"audio/wav", "audio/x-wav",
			"audio/mid", "audio/x-aiff",
			"audio/x-mpegurl", "audio/x-pn-realaudio",
			"image/jpeg", "image/x-jpeg"
			};
    
    ActionBar actionBar;
    
		@Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.main);
	        actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle(getResources().getString(R.string.TitleAbr_Playlist));

	        try {
		        Intent intent = getIntent();
		        Bundle extras = getIntent().getExtras();
		        JSONObject selectedItem = new JSONObject(extras.get(Parameter.TAG_CHANNEL_ID.getLabel()).toString());  
		    	String channelId =  selectedItem.getString(Parameter.TAG_CHANNEL_ID.getLabel());
	        	lvRadioPlayList = (ListView) findViewById(android.R.id.list);
	        	if (lvRadioPlayList != null ){
	            	RadioPlaylList playList = new RadioPlaylList(lvRadioPlayList);
	            	playList.execute(channelId);
	        	}
			} catch (Exception e) {
				Log.e("onCreate", e.getMessage());
			}
		}
		
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			   getSupportMenuInflater().inflate(R.menu.activity_radioplayer, menu);

			   return super.onCreateOptionsMenu(menu);
		}
	
	// Item Selected Menu Home Icon
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent intent = new Intent(this, RadioChannels.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			return true;
		case R.id.menuQuality:
			Toast.makeText(this, "menuQuality selected", Toast.LENGTH_SHORT).show();
			return true;
		case R.id.menuAllDownload:
			Toast.makeText(this, "menuAllDownload selected", Toast.LENGTH_SHORT).show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	public void bindButtonListener(View view, final JSONObject item) {
		
		final ImageButton button = (ImageButton) view.findViewById(R.id.btDownloadTrack);
		button.setEnabled(true);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				button.setEnabled(false);
				// Handler 
			}
		});
	}  
    
	private void downloadToLocalFile(String uriStr, String filename) throws Exception {
		URL url = new URL(Uri.encode(uriStr, ":/"));
		BufferedInputStream reader = 
				new BufferedInputStream(url.openStream());
		
    	File f = new File("audiofile.ogg");
		FileOutputStream fOut = openFileOutput("audiofile.ogg",
                Context.MODE_WORLD_READABLE);
		BufferedOutputStream writer = new BufferedOutputStream(fOut); 
		
		byte[] buff = new byte[1024]; 
		int nread;
		System.out.println("Downloading");
		while ((nread = reader.read(buff, 0, 1024)) != -1) {
			writer.write(buff, 0, nread);
		}
		writer.close();
	}

    public class RadioPlaylList extends AsyncTask<String, Void, JSONObject> {
   		 
    	private ListView lvRadioPlayList;

		public RadioPlaylList(ListView lvRadioPlayList ) {
    		this.lvRadioPlayList = lvRadioPlayList;
		}
    	
		@Override
		protected JSONObject doInBackground(String... params) {
			try {
					String channelId = params[0];
					if (channelId != null)
						return ((PromoDJApplication)getApplication()).getRadioPlayList(channelId);
			} catch (Exception e) {
				Log.e("doInBackground", e.getMessage());
			}
			return null;
		}
	
    	@Override
    	protected void onPostExecute(JSONObject aPlayList) {
			super.onPostExecute(aPlayList);
			try {
				PlayListViewAdapter adapter = new PlayListViewAdapter(PlaylistActivity.this, aPlayList);
				lvRadioPlayList.setAdapter(adapter);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			lvRadioPlayList.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent,
						android.view.View view, int position, long id) {
					JSONObject item = (JSONObject)parent.getAdapter().getItem(position);
				  	Intent intent = new Intent(getApplicationContext(), RadioActivity.class);
				  	intent.putExtra(Parameter.TAG_FILEID.getLabel(),item.toString());
				  	startActivity(intent);			
			    }
			});	
    	}

    }

    
    @Override
    protected void onRestart() {
      super.onRestart();
      Log.d(TAG, "Playlist: onRestart()");
    }

    @Override
    protected void onStart() {
      super.onStart();
      Log.d(TAG, "Playlist: onStart()");
    }

    @Override
    protected void onResume() {
      super.onResume();
      Log.d(TAG, "Playlist: onResume()");
    }

    @Override
    protected void onPause() {
      super.onPause();
      Log.d(TAG, "Playlist: onPause()");
    }

    @Override
    protected void onStop() {
      super.onStop();
      Log.d(TAG, "Playlist: onStop()");
    }

    @Override
    protected void onDestroy() {
      super.onDestroy();
      
      //clean the file cache when root activity exit
      //the resulting total cache size will be less than 3M   
      if(isTaskRoot()){
              //clean the file cache
    	  AQUtility.cleanCacheAsync(this);
      }
      
      Log.d(TAG, "Playlist: onDestroy()");
    }
}