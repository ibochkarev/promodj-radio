package ru.bislab.promodj.android.enviroment;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.bislab.promodj.android.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

public class Utility {
	
	   // Alert Dialog Manager
	   AlertDialogManager alert = new AlertDialogManager();
    public static Map<String, Object> convertToMap(JSONObject json) throws JSONException {
        Map<String, Object> result = new HashMap<String, Object>();
        Iterator iterator = json.keys();
        while (iterator.hasNext()) {
            String key = iterator.next().toString();
            Object value = json.get(key);
            result.put(key, convertToStringIfNeeded(value));
        }
        return result;
    }

    public static List<JSONObject> convertJSONArrayToOrderedList(JSONArray aArray) throws JSONException {
    	List<JSONObject> list = new ArrayList<JSONObject>();
    	if (aArray != null)
    		for(int i = 0; i < aArray.length(); i++){
    			JSONObject channel = aArray.getJSONObject(i);
    			list.add(channel); 
    		}
    	Collections.sort(list, new Comparator<JSONObject>(){
        	public int compare(JSONObject o1, JSONObject o2) {
				try {
					long fromTime1 = Long.parseLong(o1.getString(Parameter.TAG_FROMD.getLabel()));
	    			long fromTime2 = Long.parseLong(o2.getString(Parameter.TAG_FROMD.getLabel()));
	        		long res = fromTime1 - fromTime2;
			  		if (res == 0)
			  			return 0; 
			  		else if (res > 0)
			  			return 1;
			  		else 
			  			return -1;  				
				} catch (Exception e) {	}
				return 0;
        	}
        });
        return list;
    }
    
    public static Object[] convertToArray(JSONArray json) throws JSONException {
        int length = json.length();
        Object[] result = new Object[length];
        for (int i = 0; i < length; i++) {
            Object value = json.get(i);
            result[i] = convertToStringIfNeeded(value);
        }
        return result;
    }

    private static Object convertToStringIfNeeded(Object value) throws JSONException {
        if (value == null) {
            return null;
        } if (value instanceof JSONObject) {
        } if (value instanceof JSONObject) {
            return convertToMap((JSONObject) value);
        } else if (value instanceof JSONArray) {
            return convertToArray((JSONArray) value);
        } else if (value instanceof Boolean) {
            return value;
        } else if (value instanceof Number) {
            return value;
        } else {
            return value.toString();
        }
    }
 
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) return;
        int totalHeight = 0;
        int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    } 
    
    public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
              int count=is.read(bytes, 0, buffer_size);
              if(count==-1)
                  break;
              os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }
    
    public static JSONObject copyJSONObject(JSONObject dst, JSONObject src) throws JSONException {
        Iterator keys = src.keys();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            Object value = src.get(key);
            dst.put(key, value);
        }
        
        return dst;
    }
    
    protected static boolean isNetworkAvailable( Context context ) { 
       ConnectivityManager connectivity = 
    		   (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
       
       if (connectivity == null) {   
     		Log.d("isNetworkAvailable"," connectivity is null");
     		return false;
       } else {  
	       Log.d("isNetworkAvailable"," connectivity is not null");
	       NetworkInfo[] info = connectivity.getAllNetworkInfo();   
	       if (info != null) {   
	           Log.d("isNetworkAvailable"," info is not null");
               for (int i = 0; i <info.length; i++) { 
	               if (info[i].getState() == NetworkInfo.State.CONNECTED) {
	                       return true; 
	               } else 
	            	   Log.d("isNetworkAvailable"," info["+i+"] is not connected");
	            }     
	        } else
	           Log.d("isNetworkAvailable"," info is null");
       }   
       return false;
	}
    
    
	 public static boolean checkInternetConnection(final Activity mActivity)
	 {
	         Log.d("HttpTest"," ----------checkInternetConnection--------------");
	         Context context = mActivity.getApplicationContext();
	         
	         if( !isNetworkAvailable( context) )
	         {
                Log.d("HttpTest"," network is not aviavalable");
                AlertDialog.Builder builders = new AlertDialog.Builder(mActivity);
                builders.setTitle("@string/ErHttpTest");
                LayoutInflater inflater = LayoutInflater.from(mActivity);
                View convertView = inflater.inflate(R.layout.error, null);
                builders.setView(convertView);
                builders.setPositiveButton("@string/MsgErButton",  new DialogInterface.OnClickListener(){
                	
	                 public void onClick(DialogInterface dialog, int which)
	                 {
	                         mActivity.finish();
	                 }       
                });
                builders.show();
                return false;
	         } else {
	        	 System.gc(); // Gabrage collector cleaned from dead objects  
	        	 return false;
	         }
	        	 
	}

	
}