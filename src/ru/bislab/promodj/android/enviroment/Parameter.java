package ru.bislab.promodj.android.enviroment;

import java.util.HashMap;
import java.util.Map;

import android.text.format.Time;

public enum Parameter {

   TAG_PLAYLIST(1, "playlist", "List of track selected channel"),
   TAG_FILEID(2, "fileID", "ID track"),
   TAG_FROMD(3, "fromD", "Start time play track"),
   TAG_TILLD(4, "tillD", "End time play track"),
   TAG_TITLE(5, "title", "Title"),
   TAG_URL(6, "url", "URL page track"),
   TAG_DOWNLOAD(7, "download", "URL downloading track"),
   TAG_AVATAR(8, "avatar", "Avatar artist"),
   TAG_CHANNEL_ID(9, "channelID", "ID of channel"),
   TAG_CHANNELS(10, "channels", "List of channel"),
   TAG_CHANNEL_STYLE(11, "styles", "Styles of channel"),
   TAG_DESC(12, "description", "Description of channel"),
   TAG_MAX_AGE(13, "max-age", "Max list of channel"),
   TAG_CHANNEL_URL(14, "channel_url", "Max list of channel"),
   TAG_CHANNEL_LQ(15, "lq", "Low quality of channel"),
   TAG_CHANNEL_HQ(16, "hq", "Hign quality of channel"),
   TAG_QUALITY(17, "quality", "Quality"),
   TAG_TIMESTAMP(18, "stamp", "Stamp"),
   TAG_LOGO(19, "logo_small", "Logo Channel"),
   TAG_CHANNEL_T(19, "logo_small", "Logo Channel");

    private int code;
    private String label;
    private String description;
    public static final long MILLISECONDS = 1000;
    public static final long HOUR = Time.HOUR;
    public static final int IMAGE_ROUND = 15;

    //String value Fonts
    public static final String FONT_REGULAR = "HelveticaBold.ttf";
    public static final String FONT_LIGHT = "HelveticaLight.ttf";
    public static final String DIR_MUSIC = "music";
    /**
     * A mapping between the integer code and its corresponding Status to facilitate lookup by code.
     */
    private static Map<Integer, Parameter> codeToStatusMapping;

    private Parameter(int code, String label, String description) {
        this.code = code;
        this.label = label;
        this.description = description;
    }

    public static Parameter getParams(int i) {
        if (codeToStatusMapping == null) {
            initMapping();
        }
        Parameter result = null;
        for (Parameter param : values()) {
            result = codeToStatusMapping.get(i);
        }
        return result;
    }

    private static void initMapping() {
        codeToStatusMapping = new HashMap<Integer, Parameter>();
        for (Parameter s : values()) {
            codeToStatusMapping.put(s.code, s);
        }
    }

    public int getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public String toString() {
        return label;
    }

    public String getDescription() {
        return description;
    }

}