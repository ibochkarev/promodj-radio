package ru.bislab.promodj.android.view;

import ru.bislab.promodj.android.R;
import android.content.Context;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;

public class RadioActivityView extends ExpandableListView {
	
	Context context;
	public RadioActivityView(Context context) {
		super(context);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	    
	    int size = 0;
	    int width = getMeasuredWidth();
	    int height = getMeasuredHeight();
	    int widthWithoutPadding = width - getPaddingLeft() - getPaddingRight();
	    int heigthWithoutPadding = height - getPaddingTop() - getPaddingBottom();
	     
	    LinearLayout layout = (LinearLayout) RadioActivityView.this.findViewById(R.id.relativeLayout);
	    
	    LayoutParams params = (LayoutParams) layout.getLayoutParams();
	    int c =0;
	    for(int i =0; i < layout.getChildCount(); i++){
	    	View v = layout.getChildAt(i);
	    	
	    	c +=v.getHeight();
	    }
	    // set the dimensions
	    if (widthWithoutPadding > heigthWithoutPadding) {
	        size = heigthWithoutPadding;
	    } else {
	        size = widthWithoutPadding;
	    }
	 
	    LayoutParams layoutParams = (LayoutParams) getLayoutParams();
	    
	    setMeasuredDimension(size + getPaddingLeft() + getPaddingRight(), size + getPaddingTop() + getPaddingBottom());
	}
	
    /*
	private static final float RATIO = 4f / 3f;
	 
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	 
	    int width = getMeasuredWidth();
	    int height = getMeasuredHeight();
	    int widthWithoutPadding = width - getPaddingLeft() - getPaddingRight();
	    int heigthWithoutPadding = height - getPaddingTop() - getPaddingBottom();
	 
	    int maxWidth = (int) (heigthWithoutPadding * RATIO);
	    int maxHeight = (int) (widthWithoutPadding / RATIO);
	 
	    if (widthWithoutPadding  > maxWidth) {
	        width = maxWidth + getPaddingLeft() + getPaddingRight();
	    } else {
	        height = maxHeight + getPaddingTop() + getPaddingBottom();
	    }
	 
	    setMeasuredDimension(width, height);
	}*/
}
